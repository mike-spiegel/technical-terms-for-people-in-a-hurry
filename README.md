# Technical Terms for People in a Hurry

## Background
A short talk given to our sales staff to make sure we all "speak the same language" when referring to customer asks

## Quickstart

```sh
npm install
npm start
```

to install dependencies and run the presentation server

## Docs
1. see the [reveal-md](https://github.com/webpro/reveal-md#features) feature docs