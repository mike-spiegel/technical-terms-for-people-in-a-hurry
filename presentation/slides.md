# Technical Terms
(For people in a hurry) <!-- .element: class="fragment" -->
---
## What will we cover today?

1. APIs (and Interfaces)
1. Existing Integrations
    1. Email
    1. Phone
    1. JSON export
    1. Lightweight HTML markup
1. UI vs UX

---
# API


- <!-- .element: class="fragment" --> Application
- <!-- .element: class="fragment" --> Programming
- <!-- .element: class="fragment" --> Interface

Note: 
1. What is an Interface?  
1. Why might a human (user) need a different interface than a Machine?  
1. Why are interfaces difficult to change?  
1. Why are APIs difficult to change?


----

## Let's make that clearer
1. <!-- .element: class="fragment" --> What is an interface?  
1. <!-- .element: class="fragment" --> Why might a human (user) need a different interface than a machine?  

----

## API Usecases

1. Automating repetitive tasks
1. Building new features in "App A" with data from "App B"
1. Decoupling technology stacks

----


## API pitfalls

1. Why are interfaces difficult to change?  
1. Why are APIs difficult to change?
<!-- .element: class="fragment" --> 


---
## Existing Integrations
1. Email
1. Phone
1. JSON export
1. Lightweight HTML markup
1. Outlook

---

## UI vs UX

---

## Anything else?